# Smaato coding challenge api-gateway application for Extension 2

#Tech Stack used for this project
* Java 11
* Spring Boot 2.6.0
* Maven

# Getting Started
* Go to project root directory system_path/api-gateway
* Run ```mvn clean install``` this will generate the ```api-gateway-1.0.jar``` file in system_path/api-gateway/target.
* Run the jar.
* Application runs on port 8080.
* Also download and run the ```challenge``` application shared in another repo from from ```extesnion2``` branch.

#Assumptions
* If the invalid URI is sent as query param, it will be considered as error in processing and will not be counted as unique request.

#Note
* A postman collection is also shared over email to test the different scenarios of the application.

