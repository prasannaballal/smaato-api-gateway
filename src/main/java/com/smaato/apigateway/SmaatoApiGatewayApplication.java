package com.smaato.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SmaatoApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmaatoApiGatewayApplication.class, args);
	}

}
