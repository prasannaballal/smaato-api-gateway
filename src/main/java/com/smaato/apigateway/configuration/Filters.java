package com.smaato.apigateway.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.MultiValueMap;

import java.util.concurrent.ConcurrentSkipListSet;

@Configuration
public class Filters {

    private static final Logger LOGGER = LogManager.getLogger(Filters.class);
    private static ConcurrentSkipListSet<Integer> numberOfUniqueCalls = new ConcurrentSkipListSet();

    public static final String ID = "id";
    public static final String ENDPOINT = "endpoint";
    public static final String COUNT = "count";

    @Bean
    public GatewayFilter mainFilter() {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            MultiValueMap<String, String> queryParamsMap = request.getQueryParams();

            if (queryParamsMap.containsKey(ID)) {
                numberOfUniqueCalls.add(Integer.parseInt(queryParamsMap.get(ID).get(0)));
                if (queryParamsMap.containsKey(ENDPOINT)) {
                    request.mutate().header(COUNT, String.valueOf(numberOfUniqueCalls.stream().count()));
                }
            }
            return chain.filter(exchange);
        };
    }

    @Scheduled(fixedRate = 60000)
    public void writeNumberOfUniqueRequests() {
        LOGGER.info("Number of unique calls: {} ", numberOfUniqueCalls.stream().count());
        numberOfUniqueCalls.removeAll(numberOfUniqueCalls);
    }
}
