package com.smaato.apigateway.configuration;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Routes {

    public static final String API_SMAATO_ACCEPT = "/api/smaato/accept";
    public static final String HTTP_LOCALHOST = "http://localhost:8081/";

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder, GatewayFilter mainFilter) {
        return builder.routes()
                .route("r1", r ->
                        r.path(API_SMAATO_ACCEPT)
                                .filters(gatewayFilterSpec -> gatewayFilterSpec.filter(mainFilter))
                                .uri(HTTP_LOCALHOST))
                .build();
    }
}
